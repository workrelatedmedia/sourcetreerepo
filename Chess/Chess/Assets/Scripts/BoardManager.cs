﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BoardManager : MonoBehaviour {

    public Chessman[,] Chessmans { get; set; }
    private Chessman SelectedChessman;
    public static BoardManager Instance { set; get; }
    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = 0.5f;
    private int selectionX = -1;
    private int selectionY=-1;
    public List<GameObject> chessmanPrefabs;
    private List<GameObject> activeChessman=new List<GameObject>();
    public bool isWhiteTurn;
    private bool[,] AllowedMoves;

    private void Start()
    {
        SpawnAllChessmans();
        Instance = this;
    }
    private void Update()
    {
        DrawChessboard();
        UpdateSelection();

        if (Input.GetMouseButtonDown(0))
        {
            if (selectionX >= 0 && selectionY >= 0)
            {
                if (SelectedChessman == null)
                {
                    SelectChessman(selectionX, selectionY);
                }
                else
                {
                    MoveChessman(selectionX, selectionY);
                }
            }
        }
    }

    private void MoveChessman(int x, int y)
    {
        if (AllowedMoves[x, y])
        {
            Chessman c = Chessmans[x, y];

            if (c.GetType() == typeof(King))
            {
                return;
            }
            if (c != null && c.isWhite != isWhiteTurn)
            {
                activeChessman.Remove(c.gameObject);
                Destroy(c.gameObject);
            }
            Chessmans[SelectedChessman.CurrentX, SelectedChessman.CurrentY] = null;
            SelectedChessman.transform.position = GetTileCenter(x, y)+ new  Vector3(0, SelectedChessman.transform.position.y,0);
            SelectedChessman.SetPosition(x, y);
            isWhiteTurn = !isWhiteTurn;
        }
        BoardHilights.Instance.HideHilights();
        SelectedChessman = null;
    }

    private void SelectChessman(int x, int y)
    {
        if (Chessmans[x, y] == null|| Chessmans[x, y].isWhite!=isWhiteTurn)
        {
            return;
        }

        AllowedMoves = Chessmans[x, y].PossibleMove();
        SelectedChessman = Chessmans[x, y];
        BoardHilights.Instance.HighLightAllowedMoves(AllowedMoves);
    }

    private void UpdateSelection()
    {
        if (!Camera.main)
        {
            return;
        }
        //raycasts out and 'hit'
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("ChessPlane")))
        {
            Debug.Log(hit.point);
            selectionX = (int)hit.point.x;
            selectionY = (int)hit.point.z;
        }
        else
        {
            selectionY = -1;
            selectionX = -1;
        }
    }

    private void DrawChessboard()
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heightLine = Vector3.forward * 8;

        for (int i = 0; i < 9; i++)
        {
            Vector3 start = Vector3.forward * i;
            Debug.DrawLine(start, start + widthLine);

             start = Vector3.right * i;
            Debug.DrawLine(start, start + heightLine);

        }

        if (selectionX >= 0 && selectionY >= 0)
        {
            Vector3 start = Vector3.forward * selectionY + Vector3.right * selectionX;
            Vector3 end = start + Vector3.forward + Vector3.right;
            Debug.DrawLine(start, end);
            Debug.DrawLine(start + Vector3.right, end - Vector3.right);
        }
    }

    private void SpawnChessman(int index, int x, int y)
    {
        GameObject go = Instantiate(chessmanPrefabs[index], GetTileCenter(x,y)+(new Vector3(0,chessmanPrefabs[index].transform.position.y,0)), chessmanPrefabs[index].transform.rotation) as GameObject;
        go.transform.SetParent(transform);
        Chessmans[x, y] = go.GetComponent<Chessman>();
        Chessmans[x, y].SetPosition(x, y);
        activeChessman.Add(go);
    }

    private void SpawnAllChessmans()
    {
        activeChessman = new List<GameObject>();

        Chessmans = new Chessman[8, 8];
        //King
        SpawnChessman(0, 3, 0);
        SpawnChessman(1, 4, 0);
        SpawnChessman(2, 0, 0);
        SpawnChessman(2, 7, 0);
        SpawnChessman(3, 2, 0);
        SpawnChessman(3, 5, 0);
        SpawnChessman(4, 1, 0);
        SpawnChessman(4, 6, 0);
        for (int i = 0; i <= 7; i++)
        {
            SpawnChessman(5, i, 1);
        }

        //black team
        activeChessman = new List<GameObject>();
        //King
        SpawnChessman(6, 4, 7);
        SpawnChessman(7, 3, 7);
        SpawnChessman(8, 0, 7);
        SpawnChessman(8, 7, 7);
        SpawnChessman(9,2, 7);
        SpawnChessman(9, 5, 7);
        SpawnChessman(10, 1, 7);
        SpawnChessman(10, 6, 7);
        for (int i = 0; i <= 7; i++)
        {
            SpawnChessman(11, i, 6);
        }


    }

    private Vector3 GetTileCenter(int x, int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (TILE_SIZE * x) + TILE_OFFSET;
        origin.z += (TILE_SIZE * y) + TILE_OFFSET;
        return origin;
    }
}
